import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
// import { closeModalAC, openModalAC } from '../../redux/modal/modalActions';
import { toggleOpenModalAC } from '../../redux/modal/modalActions';
import Item from '../../components/item';
import CustomerForm from '../../components/form';
import Modal from '../../components/modal';
import Button from '../../components/button';

const CartScreen = ({ cartItems, onDeleteFromCart, onClearCart }) => {
    const modal = useSelector(state => state.modal.isOpened);
    const productModal = useSelector(state => state.modal.productModal);
    const dispatch = useDispatch();

    // const openModal = (product) => {
    //     dispatch(openModalAC(product));
    // };

    // const closeModal = () => {
    //     dispatch(closeModalAC());
    // };

    const toggleOpenModal = (product) => {
        dispatch(toggleOpenModalAC(product));
    };

    const handleDeleteItem = () => {
        onDeleteFromCart(productModal.id);
        toggleOpenModal(productModal.id);
    }

    const calculateTotal = (Arr) => {
        let totalSum = 0;
        Arr.forEach((el) => {
            totalSum += Number.parseFloat(el.price)
        })
        return totalSum.toFixed(3)
    }

    return (
        <div>
            <h2>Your cart</h2>
            {cartItems.length ? (
            <div>
                {cartItems.map((el) => (
                    <Item key={el.id} item={el} isCloseIcon={true} onDeleteFromCart={onDeleteFromCart} openModal={toggleOpenModal}/>
                ))}
                <b style={{display: "block", textAlign: "center", margin: "36px 0px 60px 0px"}}>
                    Total: UAH {calculateTotal(cartItems)}
                </b>
                <CustomerForm cart={cartItems} onClearCart={onClearCart}/>
            </div>) : <p style={{ textAlign: "center" }}>You cart is empty!</p>
            }

            {modal && (
                    <div className='Modal' onClick={toggleOpenModal}>
                        <Modal header="Are you sure?" text="Do you really want to delete this product from a cart?" actions={<><Button text='Ok' onClick={handleDeleteItem}/><Button text="Cancel" onClick={toggleOpenModal} /></>} />
                    </div>
                )}
        </div>
    );
}

export default CartScreen;