import { createContext, useMemo, useState } from "react";

export const ViewContext = createContext();

export const ViewProvider = ({children}) => {
    const [cardView, setCardView] = useState(true);

    const value = useMemo(() => ({cardView, setCardView}), [cardView]);

    return (
        <ViewContext.Provider value={value}>
            {children}
        </ViewContext.Provider>
    )
}
