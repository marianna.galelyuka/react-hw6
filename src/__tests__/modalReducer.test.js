import store from "../redux/store";
import modalReducer from "../redux/modal/modalReducer";
import { toggleOpenModalAC } from "../redux/modal/modalActions";

describe ("Testing Modal Reducer", () => {
    const initialState = store.getState().modal;
    
    test ("TOGGLE_MODAL", () => {
        expect(modalReducer(initialState, toggleOpenModalAC({"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple", "price": "51.999"}))).toEqual({
            isOpened: true,
            productModal: {"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple", "price": "51.999"},
        });

        expect(modalReducer({isOpened: true, productModal: {"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple", "price": "51.999"}}, toggleOpenModalAC({"id": 2, "productName": "Apple iPhone 14 Pro Max 128GB Gold", "price": "51.699"}))).toEqual({
            isOpened: false,
            productModal: {"id": 2, "productName": "Apple iPhone 14 Pro Max 128GB Gold", "price": "51.699"},
        });

        expect(modalReducer({isOpened: true, productModal: {"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple", "price": "51.999"}}, toggleOpenModalAC())).toEqual({
            isOpened: false,
            productModal: undefined,
        });
    })
})