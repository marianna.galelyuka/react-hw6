import { render, screen } from "@testing-library/react";
import Modal from "../components/modal";

describe ("Testing Modal", () => {
    test (`should render one modal without props`, () => {
        render (<Modal />);
        const modal = screen.getByTestId("modal");
        expect(modal).toBeInTheDocument();
        expect(modal).toHaveTextContent("");
        
        const modalContent = screen.getByTestId("modal-content");
        expect(modalContent).toBeInTheDocument();
        expect(modalContent).toHaveTextContent("");

        const modalHeader = screen.getByTestId("modal-header");
        expect(modalHeader).toBeInTheDocument();
        expect(modalHeader).not.toHaveTextContent("This is modal header");

        const modalList = screen.getAllByTestId("modal");
        expect(modalList).toHaveLength(1);
    })

    test (`modal should have header`, () => {
        render (<Modal header="THIS IS MODAL"/>);
        const header = screen.getByTestId("modal-header");
        expect(header).toBeInTheDocument();
        expect(header).toHaveTextContent(/this is modal/i);
    })

    test (`modal should have text content`, () => {
        render (<Modal text="THIS IS MODAL CONTENT"/>);
        const text = screen.queryByText(/this is modal content/i);
        expect(text).toBeInTheDocument();
        const textInv = screen.queryByText(/this is product content/i);
        expect(textInv).not.toBeInTheDocument();    
    })

    test (`modal should have action buttons`, async () => {
        render (<Modal actions={
            <>
            <button> OK </button>,
            <button> CANCEL </button>
            </>
            }
        />);
        const buttonsList = await screen.findAllByRole("button");
        expect(buttonsList).toHaveLength(2);
        expect(buttonsList).toMatchSnapshot();
    })

    test('Add-to-cart modal snapshot', () => {
        render (<Modal header="Do you want to add this product to a cart?" text="Adding a product to a cart doesn't mean its reservation. Do you want to continue?" actions={<><button> OK </button>,<button> CANCEL </button></>} />)
        const modal = screen.getByTestId("modal");
        expect(modal).toMatchSnapshot();
    })
})