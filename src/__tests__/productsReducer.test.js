import store from "../redux/store";
import productsReducer from "../redux/products/productsReducer";
import { fetchProductsErrorAC, fetchProductsRequestAC, fetchProductsSuccessAC } from "../redux/products/productsActions";

describe ("Testing Selector", () => {
    test ("products should be an empty array at the beginning", () => {
        const products = store.getState().products.products;
        expect(products).toEqual([]);
    })

    test ("loading should be false at the beginning", () => {
        const loading = store.getState().products.loading;
        expect(loading).toBe(false);
    })

    test ("error should be null at the beginning", () => {
        const error = store.getState().products.error;
        expect(error).toBe(null);
    })

    test ("loading started", () => {
        const getProductsLoading = (state) => state.loading;
        // const isLoading = getProductsLoading({ loading: true, products: [], error: null });
        // expect(isLoading).toBe(true);
        expect(getProductsLoading({ loading: true, products: [], error: null })).toBe(true);
    })
})

describe ("Testing Products Reducer", () => {
    // const initialState = {
    //     products: [],
    //     loading: false,
    //     error: null,
    // };
    const initialState = store.getState().products;

    test ("FETCH_PRODUCTS_REQUEST", () => {
        expect(productsReducer(initialState, fetchProductsRequestAC())).toEqual({
            products: [],
            loading: true,
            error: null,
        });
    })

    test ("FETCH_PRODUCTS_SUCCESS", () => {
        const products = [
            {"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple", "price": "51.999"},
            {"id": 2, "productName": "Apple iPhone 14 Pro Max 128GB Gold", "price": "51.699"},
            {"id": 3, "productName": "Apple iPhone 14 Pro Max 128GB Space Black", "price": "50.999"}
        ]
        expect(productsReducer(initialState, fetchProductsSuccessAC(products))).toEqual({
            products: [
                {"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple", "price": "51.999"},
                {"id": 2, "productName": "Apple iPhone 14 Pro Max 128GB Gold", "price": "51.699"},
                {"id": 3, "productName": "Apple iPhone 14 Pro Max 128GB Space Black", "price": "50.999"}
            ],
            loading: false,
            error: null,
        });
    })

    test ("FETCH_PRODUCTS_ERROR", () => {
        expect(productsReducer(initialState, fetchProductsErrorAC("There is no such page"))).toEqual({
            products: [],
            loading: false,
            error: "There is no such page",
        });
    })

    
    test ("FETCH_PRODUCTS with undefined state", () => {
        expect(productsReducer(undefined, fetchProductsRequestAC())).toEqual({
            products: [],
            loading: true,
            error: null,
        });

        expect(productsReducer(undefined, fetchProductsSuccessAC([
            {"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple"},
            {"id": 2, "productName": "Apple iPhone 14 Pro Max 128GB Gold"},
            {"id": 3, "productName": "Apple iPhone 14 Pro Max 128GB Space Black"}
        ]))).toEqual({
            products: [
                {"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple"},
                {"id": 2, "productName": "Apple iPhone 14 Pro Max 128GB Gold"},
                {"id": 3, "productName": "Apple iPhone 14 Pro Max 128GB Space Black"}
            ],
            loading: false,
            error: null,
        });

        expect(productsReducer(undefined, fetchProductsErrorAC("There is no such page"))).toEqual({
            products: [],
            loading: false,
            error: "There is no such page",
        });
    })

    
    test ("FETCH_PRODUCTS with an empty state", () => {
        expect(productsReducer({}, fetchProductsRequestAC())).toEqual({
            loading: true,
            error: null,
        });

        expect(productsReducer({}, fetchProductsSuccessAC([
            {"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple"},
            {"id": 2, "productName": "Apple iPhone 14 Pro Max 128GB Gold"},
            {"id": 3, "productName": "Apple iPhone 14 Pro Max 128GB Space Black"}
        ]))).toEqual({
            products: [
                {"id": 1, "productName": "Apple iPhone 14 Pro Max 128GB Deep Purple"},
                {"id": 2, "productName": "Apple iPhone 14 Pro Max 128GB Gold"},
                {"id": 3, "productName": "Apple iPhone 14 Pro Max 128GB Space Black"}
            ],
            loading: false,
        });

        expect(productsReducer({}, fetchProductsErrorAC("There is no such page"))).toEqual({
            loading: false,
            error: "There is no such page",
        });
    })
})