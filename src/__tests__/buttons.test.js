import { render, screen, fireEvent } from "@testing-library/react";
import Button from "../components/button";

describe ("Testing Button", () => {
    test (`should render one button`, async () => {
        render (<Button />);
        const buttonList = await screen.findAllByRole("button");
        expect(buttonList).toHaveLength(1);
    })

    // test (`button should have text content`, async () => {
    //     render (<Button />);
    //     const button = await screen.findByRole("button");
    //     expect(button).toHaveTextContent("");
    // })
    
    // test (`button should have onClick property`, async () => {
    //     render (<Button />);
    //     const button = await screen.findByRole("button");
    //     expect(button).toHaveProperty("onclick");
    // })
    
    // test (`button should have an attribute "style"`, async () => {
    //     render (<Button />);
    //     const button = await screen.findByRole("button");
    //     expect(button).toHaveAttribute("style");
    // })
    test (`button should accept the props`, async () => {
        render (<Button />);
        const button = await screen.findByRole("button");
        expect(button).toHaveTextContent("");
        expect(button).toHaveProperty("onclick");
        expect(button).toHaveAttribute("style");
    })

    test (`button has background style`, async () => {
        render (<Button backgroundColor="blue"/>);
        const button = await screen.findByRole("button");
        expect(button).toHaveStyle({backgroundColor: "blue"});
        expect(button).toMatchSnapshot();
    })

    test (`button has inner text`, async () => {
        render (<Button text="It's Test Button" />);
        const button = await screen.findByRole("button");
        expect(button).toBeInTheDocument(/"it's test button"/i);
    })

    test (`button click event`, async () => {
        let isActive = false;
        const toggleActive = () => {
            isActive = !isActive;
        }

        render (<Button onClick={toggleActive} />);
        const button = await screen.findByRole("button");
        fireEvent.click(button);
        expect(isActive).toBe(true);
        fireEvent.click(button);
        expect(isActive).toBe(false);
    })

    test ('Add-to-cart button snapshot', async () => {
        render (<Button text="Add to cart" className="product__toCartBtn" backgroundColor="yellow" />);
        const addToCartBtn = await screen.findByRole("button");
        expect(addToCartBtn).toMatchSnapshot();
    })
})