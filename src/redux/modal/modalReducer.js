// import { OPEN_MODAL, CLOSE_MODAL } from "./modalTypes";
import { TOGGLE_OPEN_MODAL } from "./modalTypes";

const initialModalState = {
    isOpened: false,
    productModal: '',
};

const modalReducer = (state = initialModalState, action) => {
    switch (action.type) {
        // case OPEN_MODAL:
        //     return {
        //         ...state,
        //         isOpened: true,
        //         productModal: action.payload,
        //     }
        // case CLOSE_MODAL:
        //     return {
        //         ...state,
        //         isOpened: false,
        //     }
        case TOGGLE_OPEN_MODAL:
            return {
                ...state,
                isOpened: !state.isOpened,
                productModal: action.payload,
            }
        default:
            return state
    }
};

export default modalReducer;