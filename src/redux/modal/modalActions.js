// import { OPEN_MODAL, CLOSE_MODAL, TOGGLE_MODAL } from "./modalTypes";
import { TOGGLE_OPEN_MODAL } from "./modalTypes";

// export const openModalAC = (product) => ( {
//     type: OPEN_MODAL,
//     payload: product,
// })
  
// export const closeModalAC = () => ({
//     type: CLOSE_MODAL,
// });

export const toggleOpenModalAC = (product) => ( {
    type: TOGGLE_OPEN_MODAL,
    payload: product,
})