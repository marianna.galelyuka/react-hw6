import { FETCH_PRODUCTS_ERROR, FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_SUCCESS } from './productsTypes';

export const fetchProductsRequestAC = () => ({
    type: FETCH_PRODUCTS_REQUEST,
})

export const fetchProductsSuccessAC = (products) => ({
    type: FETCH_PRODUCTS_SUCCESS,
    payload: products,
})

export const fetchProductsErrorAC = (error) => ({
    type: FETCH_PRODUCTS_ERROR,
    payload: error,
})