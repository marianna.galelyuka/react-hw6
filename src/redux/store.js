import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import productsReducer from './products/productsReducer';
import modalReducer from './modal/modalReducer';

const rootReducer = combineReducers({
    products: productsReducer,
    modal: modalReducer,
});

// const store = createStore(rootReducer, applyMiddleware(thunk, logger));

const composedEnhancer = composeWithDevTools(
    applyMiddleware(thunk),
    applyMiddleware(logger),
)

const store = createStore(rootReducer, composedEnhancer);

export default store;