import React from "react";
import Button from '../button';
import PropTypes from "prop-types";

import styles from './Modal.module.css';

const Modal = (props) => {
    return (
        <div data-testid="modal" className={styles.modal__content} >
            <div className={styles.container} onClick={e => e.stopPropagation()}>
                <div className={styles.modal__title}>
                    <h3 data-testid="modal-header" className={styles.modal__header}>{props.header}</h3>
                </div>
                <p data-testid="modal-content" className={styles.modal__text}>{props.text}</p>
                <div className={styles.modal__buttons}>
                    {props.actions}
                </div>
            </div>
        </div>
    )
}

Button.propTypes = {
    isOpen: PropTypes.bool,
    closeButton: PropTypes.bool,
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.object,
};

Button.defaultProps = {
    closeButton: false,
};

export default Modal;