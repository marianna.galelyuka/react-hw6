import React from 'react';
import { IoClose } from "react-icons/io5";

import styles from './Item.module.css';

const Item = ({ item, isCloseIcon, openModal }) => {
    return (
        isCloseIcon ?
            <div className={styles.item}>
                <img src={item.imgUrl} alt='Item' className={styles.item__img}></img>
                <div className={styles.item__info}>
                    <div className={styles.item__details}>
                        <p className={styles.item__details_name}>{item.productName}</p>
                        <p className={styles.item__details_price}>UAH {item.price}</p>
                    </div>
                    <IoClose className={styles.item__deleteIcon} onClick={() => (openModal(item))}/>
                </div>
            </div> :
                <div className={styles.item}>
                    <img src={item.imgUrl} alt='Item' className={styles.item__img}></img>
                    <div className={styles.item__details}>
                        <p className={styles.item__details_name}>{item.productName}</p>
                        <p className={styles.item__details_price}>UAH {item.price}</p>
                    </div>
                </div>
    )
}

export default Item;