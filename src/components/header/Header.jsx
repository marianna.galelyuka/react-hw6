import React from "react";
import { FaStar, FaShoppingCart } from "react-icons/fa"
import { NavLink } from 'react-router-dom';
import PropTypes from "prop-types";

import styles from './Header.module.css'; 

const Header = ({favorites, cart}) => {
    return (
        <header className={styles.header}>
            <div className={styles.header__logo}>
                <a className={styles.header__logo_link} href="/">
                    <img alt="isweet.com.ua" className={styles.header__logo_img} width="200" height="100" src="https://isweet.com.ua/content/images/2/200x100l90nn0/75227810382232.webp"/>
                </a>
            </div>
            <nav className={styles.header__icons}>
                <NavLink to={'/'} className={styles.header__products}>Products</NavLink>
                <NavLink to={'/favorites'} className={styles.header__icon_fav}>
                    <div>
                        {favorites.length === 0 ?
                            <FaStar className={styles.icon__fav} /> :
                            <FaStar className={styles.icon__fav_active}/>
                        }
                        <span className={styles.icon__fav_number}>{favorites.length}</span>
                    </div>
                </NavLink>

                <NavLink to={'/cart'} className={styles.header__icon_cart}>
                    <div>
                        {cart.length === 0 ?
                            <FaShoppingCart className={styles.icon__cart} /> :
                            <FaShoppingCart className={styles.icon__cart_active}/>
                        }
                        <span className={styles.icon__cart_number} >{cart.length}</span>
                    </div>
                </NavLink>
            </nav>
        </header>
    );
}

Header.propTypes={
    favorites: PropTypes.array,
    cart: PropTypes.array
}

export default Header;